# Number system converter

Program converts real positive numbers from one system to another.

Tags: `Python`, `Tkinter`

## Running

Install requirments and run:

```python
python3 main.py
```
