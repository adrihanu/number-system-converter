# -*- coding: utf-8 -*-
#Program converts real positive numbers from one numeral system to another
from Tkinter import *

#Sequence containing all needed chars 
chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

#Function changing number from input system to decimal system
def in_to_dec(num, in_sys):
	dec = 0 
	position = len(num.split(".")[0]) - 1
	for char in num.replace(".", ""):
		dec+=chars.index(char) * in_sys**position
		position-=1
	return dec

#Function changing number from decimal system to output system 
def dec_to_out(dec, out_sys):
	dec_int, dec_fra = divmod(dec, 1)
	out_int = out_fra = ""
	while(True):#Integer
		dec_int, value = divmod(int(dec_int), out_sys)
		out_int = chars[value] + out_int
		if dec_int==0: break
	for x in range(20):#Fraction
		dec_fra *= out_sys
		value,dec_fra = divmod(dec_fra, 1)
		out_fra += chars[int(value)]
	return out_int+"."+out_fra


def compute():
	#Reading input system
	in_sys = field_in_sys.get()
	if in_sys.isdigit(): in_sys = int(in_sys) 
	else: value_text.set("Input system must be natural number!");return
	if in_sys not in range(2,len(chars)+1): 
		value_text.set("Input system must be in range from 2 to "+str(len(chars)));return

	#Reading input number
	num = field_number.get().replace(",", ".").upper()
	bad_char = [char for char in num.replace(".", "",1) if (char not in chars or chars.index(char)>=in_sys)]
	if bad_char: 
		value_text.set("Incorrect chars for input system: " + str(bad_char));return

	#Reading output system
	out_sys = field_out_sys.get()
	if out_sys.isdigit(): out_sys = int(out_sys) 
	else: value_text.set("Output system must be natural number!"); return
	if out_sys not in range(2,len(chars) + 1): 
		value_text.set("Input system must be in range from 2 to " + str(len(chars)));return
	
	#Calling functions
	dec = in_to_dec(num, in_sys)
	number_wyj = dec_to_out(dec, out_sys)

	#Result display
	value_text.set("\n{}({}) = {}({})".format(num.rstrip("."), in_sys, number_wyj.rstrip("0").rstrip("."), out_sys))

#my_window
my_window=Tk()

#labels
lab_1=Label(my_window,text="Input system: ")
lab_2=Label(my_window,text="Number: ")
lab_3=Label(my_window,text="Output system: ")

#input fields
field_in_sys=Entry(my_window)
field_number=Entry(my_window)
field_out_sys=Entry(my_window)

#my_button
my_button=Button(my_window,text="compute",command=compute)

#display field
value_text=StringVar()
field_result=Message(my_window,width=500,textvariable=value_text)

#window grid
lab_1.grid(row=0,column=0)
field_in_sys.grid(row=0,column=1)
lab_2.grid(row=1,column=0)
field_number.grid(row=1,column=1)
lab_3.grid(row=2,column=0)
field_out_sys.grid(row=2,column=1)
my_button.grid(column=1)
field_result.grid(columnspan=2)

#main loop
my_window.mainloop()
